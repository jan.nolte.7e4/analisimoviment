using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("AnalisiMoviment");
        }
    }
    private void Update()
    {
        if(GameObject.FindGameObjectsWithTag("COIN").Length == 0)
        {
            SceneManager.LoadScene("AnalisiMoviment");
        }
    }
}
